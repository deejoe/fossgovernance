# Steps for a new release

- [ ] Ensure all issues are complete in the milestone
- [ ] Add new contributors to `_pages/contributors.md`
- [ ] Update `_pages/release-notes.md`
- [ ] Create a release announcement blog post
- [ ] Ensure a milestone exists for the next release
- [ ] Close out the milestone for this release

/label ~"website"
