---
title: Documentation
layout: single
permalink: /documentation/
---

* [Getting Started](/getting-started/): How to get started using the FOSS Governance Collection
* [Contributing](/contribute/): How to contribute to the project
* [Release notes](/release-notes/)

Many thanks to [our contributors](/contributors/)! We couldn't do this without you! 🥰
