---
title: Release notes
layout: single
permalink: /release-notes/
---

---

## 2020-10

### Summary

Added 57 new documents to the collection (not all reflected in the issues) and 8 new contributors.

### Closed issues

[Release Milestone](https://gitlab.com/fossgovernance/fossgovernance/-/milestones/2)

* [SPDX governance](https://gitlab.com/fossgovernance/fossgovernance/-/issues/25)
* [SFOSC](https://gitlab.com/fossgovernance/fossgovernance/-/issues/26)
* [SFOSC trademark policy](https://gitlab.com/fossgovernance/fossgovernance/-/issues/27)
* [SFOSC voting policy](https://gitlab.com/fossgovernance/fossgovernance/-/issues/28)
* [SPI trademarks](https://gitlab.com/fossgovernance/fossgovernance/-/issues/29)
* [XBMC Foundation - Trademark Policy](https://gitlab.com/fossgovernance/fossgovernance/-/issues/33)
* [XBMC Foundation - Distribution Policy](https://gitlab.com/fossgovernance/fossgovernance/-/issues/34)
* [Kodi - Code of Conduct](https://gitlab.com/fossgovernance/fossgovernance/-/issues/35)
* [XBMC Foundation - Trademarks](https://gitlab.com/fossgovernance/fossgovernance/-/issues/36)
* [Convert the Getting Started post to documentation](https://gitlab.com/fossgovernance/fossgovernance/-/issues/41)
* [Add "Django Code of Conduct - Reporting Guide"](https://gitlab.com/fossgovernance/fossgovernance/-/issues/42)
* [Adding Apache Software Foundation Bylaws document](https://gitlab.com/fossgovernance/fossgovernance/-/issues/43)
* [Free Software Foundation Europe - Code of Conduct](https://gitlab.com/fossgovernance/fossgovernance/-/issues/44)
* [OpenStack Foundation - Project Confirmation Guidelines](https://gitlab.com/fossgovernance/fossgovernance/-/issues/46)
* [Parrot Foundation - Bylaws](https://gitlab.com/fossgovernance/fossgovernance/-/issues/47)
* [Parrot Foundation - Conflict of Interest Policy](https://gitlab.com/fossgovernance/fossgovernance/-/issues/48)
* [Parrot Foundation - Annual Conflict of Interest Statement](https://gitlab.com/fossgovernance/fossgovernance/-/issues/49)
* [Parrot Foundation - Contributor License Agreement](https://gitlab.com/fossgovernance/fossgovernance/-/issues/50)
* [The Perl Foundation - Bylaws](https://gitlab.com/fossgovernance/fossgovernance/-/issues/51)
* [The Perl Foundation - Contributor License Agreement](https://gitlab.com/fossgovernance/fossgovernance/-/issues/52)
* [The Perl Foundation - Conference Standards of Conduct](https://gitlab.com/fossgovernance/fossgovernance/-/issues/53)
* [Nordix Foundation (USA) - Bylaws](https://gitlab.com/fossgovernance/fossgovernance/-/issues/54)
* [OpenStack listings under confusingly different names](https://gitlab.com/fossgovernance/fossgovernance/-/issues/55)
* [Parrot Foundation - Articles of Incorporation](https://gitlab.com/fossgovernance/fossgovernance/-/issues/56)
* [Software in the Public Interest - Associated Project Policies](https://gitlab.com/fossgovernance/fossgovernance/-/issues/57)
* [Software in the Public Interest - Bylaws](https://gitlab.com/fossgovernance/fossgovernance/-/issues/58)
* [Entry for non-existent Ubuntu CLA](https://gitlab.com/fossgovernance/fossgovernance/-/issues/59)
* [OpenWrt](https://gitlab.com/fossgovernance/fossgovernance/-/issues/60)
* [OpenWrt Trademarks](https://gitlab.com/fossgovernance/fossgovernance/-/issues/61)
* [Propose MindSpore community governance document to be added into the collection](https://gitlab.com/fossgovernance/fossgovernance/-/issues/62)
* [Update contributor doc: ask before working on an issue](https://gitlab.com/fossgovernance/fossgovernance/-/issues/66)
* [Create a new Contributors/Thanks page](https://gitlab.com/fossgovernance/fossgovernance/-/issues/68)

### New contributors

* [Allison Randal](https://gitlab.com/allisonrandal)
* [D. Joe](https://gitlab.com/deejoe)
* [Danny Abukalam](https://gitlab.com/dabukalam)
* [Katie McLaughlin](https://gitlab.com/glasnt)
* [Max Mehl](https://gitlab.com/mxmehl)
* [pkerling](https://gitlab.com/pkerling)
* [Swapnil](https://gitlab.com/swapnilmmane)
* [Zhipeng Huang](https://gitlab.com/hannibalhuang)
