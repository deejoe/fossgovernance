---

title: "Getting started with the FOSS Governance Collection"
date: 2020-10-12
author: vmbrasseur
excerpt: "Here are a few quick tips to help you get started with using the FOSS Governance Collection."
layout: single
permalink: /getting-started

---
This collection contains hundreds of governance documents from free/open source software (FOSS) projects. These documents range from voting, trademark, and privacy policies to mission statements and codes of conduct. Any documentation around the human infrastructure of a FOSS project is fair game for the collection.

The collection itself is on [Zotero](https://www.zotero.org/groups/2310183/foss_governance/items), an excellent Free Software tool and service that's usually used for research and citations. It also happens to be perfect for something like this, because it allows us to tag and catalogue the collection while also providing snapshots of each governance document.

<a href="https://www.zotero.org/groups/2310183/foss_governance/items"><img src="/assets/images/posts/zotero-tags.png" alt="A screenshot of the tag selection widget on Zotero" title="A screenshot of the tag selection widget on Zotero" align="left" width="200" style="margin-right: 10px;" /></a> One of the quickest ways to find what you're looking for in the collection is to use the tag selection widget in the lower left of the Zotero page. Select the tags of interest to you and the collection display will filter to show only those documents that apply to those tags. As you select tags, the tag list will also filter to show only those tags that occur with the one(s) you selected. Select a tag again to remove it from the filter. You can also search the tags using the _Filter Tags_ field below the tag list.

<a href="https://www.zotero.org/groups/2310183/foss_governance/items"><img src="/assets/images/posts/zotero-search.png" alt="A screenshot of the search widget on Zotero, showing the option to include full-text in the search" title="A screenshot of the search widget on Zotero, showing the option to include full-text in the search" align="left" width="200" style="margin-right: 10px;" /></a>Search is also a good way to locate information in the collection. One of the big advantages of Zotero is that not only will it take snapshots of every document in the collection, it also indexes those snapshots so you can search them. Select the _Title, Creator, Year + Full-Text_ option from the search dropdown to do this sort of document deep dive.

While Zotero is available on the web, it also offers [desktop clients for Linux, Mac, and Windows](https://www.zotero.org/download/). Using a desktop client means that you can use the collection even when you're offline.

Naturally, the FOSS Governance Collection is an open project, released under the [Creative Commons Attribution](http://creativecommons.org/licenses/by/4.0/) (CC BY) v4.0 license. The website and documentation all live on [GitLab](https://gitlab.com/fossgovernance/fossgovernance), and [contributions](https://fossgovernance.org/contribute/) are welcome!

The most useful form of contribution you can make right now is suggesting more documents to add to the collection. As I write this the collection already contains 164 governance documents, but the more we add the more useful it becomes. To suggest a document to add to the collection, simply [open an issue](https://gitlab.com/fossgovernance/fossgovernance/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and select the _Suggest a document_ issue template. We'll take it from there.

If you find the collection useful, please let us know! We're eager to hear how people use this information! Simply [open an issue](https://gitlab.com/fossgovernance/fossgovernance/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) with your stories and feedback.

So what are you waiting for? Go have a look and let us know what you think!
